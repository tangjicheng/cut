package main

/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 32 * 1024 * 1024 // 32MB

char* cut(const char *start_time, const char *end_time, const char *log_file_path) {
    char *buffer = (char*)malloc(BUFFER_SIZE);
    if (buffer == NULL) {
        perror("Unable to allocate buffer");
        return NULL;
    }
    size_t buffer_pos = 0;

    buffer[0] = '\0'; // Initialize buffer to an empty string

    FILE *file = fopen(log_file_path, "r");
    if (!file) {
        perror("Error opening file");
        free(buffer);
        return NULL;
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int capture = 0;

    while ((read = getline(&line, &len, file)) != -1) {
        if (strstr(line, start_time) != NULL) {
            capture = 1;
        }
        if (capture) {
            size_t line_len = strlen(line);
            if (buffer_pos + line_len < BUFFER_SIZE - 1) {
                memcpy(&buffer[buffer_pos], line, line_len);
                buffer_pos += line_len;
                buffer[buffer_pos] = '\0'; // Null-terminate the string
            } else {
                fprintf(stderr, "Buffer overflow\n");
                break;
            }
        }
        if (strstr(line, end_time) != NULL) {
            capture = 0;
            break;
        }
    }

    if (line) {
        free(line);
    }

    fclose(file);
    return buffer;
}
*/
import "C"
import (
	"flag"
	"fmt"
	"unsafe"
)

func main() {
	startTimeStr := flag.String("start", "", "Start time in format MM/DD/YYYY HH:MM:SS")
	endTimeStr := flag.String("end", "", "End time in format MM/DD/YYYY HH:MM:SS")
	logFilePath := flag.String("file", "", "Path to the log file")
	flag.Parse()

	if *startTimeStr == "" || *endTimeStr == "" || *logFilePath == "" {
		fmt.Println("Usage: ./cut -start=\"MM/DD/YYYY HH:MM:SS\" -end=\"MM/DD/YYYY HH:MM:SS\" -file=\"path/to/logfile.log\"")
		return
	}

	startTimeC := C.CString(*startTimeStr)
	endTimeC := C.CString(*endTimeStr)
	logFilePathC := C.CString(*logFilePath)
	defer C.free(unsafe.Pointer(startTimeC))
	defer C.free(unsafe.Pointer(endTimeC))
	defer C.free(unsafe.Pointer(logFilePathC))

	buffer := C.cut(startTimeC, endTimeC, logFilePathC)
	if buffer != nil {
		defer C.free(unsafe.Pointer(buffer))
		fmt.Print(C.GoString(buffer))
	}
}
