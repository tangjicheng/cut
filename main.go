package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"strings"
)

const bufferSize = 32 * 1024 * 1024 // 32MB

func cut(startTime, endTime, logFilePath string) (string, error) {
	file, err := os.Open(logFilePath)
	if err != nil {
		return "", fmt.Errorf("error opening file: %w", err)
	}
	defer file.Close()

	var builder strings.Builder
	scanner := bufio.NewScanner(file)
	buf := make([]byte, bufferSize)
	scanner.Buffer(buf, bufferSize)

	capture := false

	for scanner.Scan() {
		lineStr := scanner.Text()

		if strings.Contains(lineStr, startTime) {
			capture = true
		}

		if capture {
			builder.WriteString(lineStr + "\n")
		}

		if strings.Contains(lineStr, endTime) {
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return "", fmt.Errorf("error reading file: %w", err)
	}

	return builder.String(), nil
}

func main() {
	startTimeStr := flag.String("start", "", "Start time in format MM/DD/YYYY HH:MM:SS")
	endTimeStr := flag.String("end", "", "End time in format MM/DD/YYYY HH:MM:SS")
	logFilePath := flag.String("file", "", "Path to the log file")
	cpuProfile := flag.String("cpuprofile", "", "write cpu profile to file")
	flag.Parse()

	if *startTimeStr == "" || *endTimeStr == "" || *logFilePath == "" {
		log.Fatal("Usage: ./cut -start=\"MM/DD/YYYY HH:MM:SS\" -end=\"MM/DD/YYYY HH:MM:SS\" -file=\"path/to/logfile.log\" -cpuprofile=\"path/to/cpuprofile\"")
	}

	if *cpuProfile != "" {
		f, err := os.Create(*cpuProfile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close()
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	result, err := cut(*startTimeStr, *endTimeStr, *logFilePath)
	if err != nil {
		log.Fatalf("Failed to extract log content: %v", err)
	}

	fmt.Print(result)
}
